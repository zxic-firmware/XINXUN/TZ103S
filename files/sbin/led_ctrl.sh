#!/bin/sh

main()
{
    echo timer > /sys/devices/platform/leds-gpio.1/leds/syspwr_led/trigger
    echo 1000 > /sys/devices/platform/leds-gpio.1/leds/syspwr_led/delay_on
    echo 1000 > /sys/devices/platform/leds-gpio.1/leds/syspwr_led/delay_off

    echo 0 > /sys/devices/platform/leds-gpio.1/leds/wifi_led/brightness

    echo 0 > /sys/devices/platform/leds-gpio.1/leds/modem_4g_led/brightness
}

main